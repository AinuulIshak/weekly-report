# Weekly Report
# Week 2

- **Agenda and Goals**

We were split into groups and held meetings through Discord during our first meeting with our subsystems to broadly set out our objective for the semester by generating a Gantt chart for each subsystem.

- **Problem and Solution**

We didn't have any problem at this point. However, we decided jointly how the Gantt chart would appear and how we would expect to work during the semester - what we'll do, when we would do it, and how we would carry out those plans. Up to this point, we've just agreed to create a Gantt chart that appears realistic because we weren't sure what more we could accomplish at the time. We never inspected the inventory as a whole subsystem to see what works and what does not, so we just drew the Gantt chart, ensuring there are tolerances between weeks in case any problems arose. I learned how to properly construct Gantt charts.

- **Work Progress**

The Gantt chart, on the other hand, is a draught and not a complete form. As a result, certain modifications may occur, and some agenda items may appear earlier or later. We intend to visit the propulsion lab the following week to examine for ourselves the equipment available for testing. We want to conduct our thrust test in Week 3.

# Week 3

- **Agenda and Goals**

For subsystem, our team went to the propulsion lab on Wednesday (3/11/2021) to understand how to use the thrust test experimental setup. Dr. Ezanee was on hand to show us how to use the equipment.

For group, Weekly group update on Friday (5/11/2021) at IDP class and appointed Irfan as checker, Mariam as process monitor, Hani as recorder, and Nava as coordinator.

- **Problem and Solution**

For subsystem, we had difficulty determining the diameter of the propeller utilised to deliver thrust to the airship during the propulsion thrust test. As a result, we put 17-inch and 23-inch propellers on a sample motor in the propulsion lab, but the motor couldn't handle the 23-inch propeller and burned. As a result, we've decided not to deal with the 23-inch propeller any longer because it will cause damage at only approximately half throttle. As previously stated, when employing the 23-inch propeller, the motor burned even before reaching 50% of the overall maximum throttle it could attain. As a result, we have decided - for safety considerations - not to use the 23-inch propeller in the future. Unless the manufacturer's specification sheet says that it is possible.

For group, choosing roles for each member was one of the decisions we made in the mixed group. It wasn't a difficult chore, though, because we proposed a duty roster in which we would rotate our turns every week. Having a duty roster that rotates is an excellent method to ensure that every team member has the opportunity to be in control of something.

- **Work Progress**

I spent the entire week learning how to utilise RCBenchmark, the software and test kit used to perform the thrust test. Week 3 was essentially an introduction to how to use the test gear. Obviously, the burnt motor caused the problem. We could improve that by actually verifying the manufacturer's specification sheet, which would give a range of measurements for the recommended sized propellers that the motor can take.
We want to repeat the thrust test the following week using motors readily available in the propulsion lab that have similar specs to the motor that we have ordered (T-Motor MN5212 KV340).

# Week 4

- **Agenda and Goals***

For subsystem, the propulsion crew went to the lab on Wednesday (10/11/2021) to test the performance of the motors. The purpose of testing the motors was to become acquainted with the software's system. We were explained how to do the test and record the data on the software (RCBenchmark). It will be regular activity, therefore we must be aware of the procedures and adhere to them in order to achieve the greatest outcomes. Reading from the textbook Fundamentals of Airship Design Volume 2.

For group, I was assigned to be the process monitor. I was in charge of making sure that our meeting ran smoothly. Every agenda item was completed. I've started discussions with each subsystem, and I've asked them to provide an update on their progress for the week.

- **Problem and Solution**

For subsystem, We made the decision to keep testing the motors. This was a simple decision to make following a virtual discussion because we had previously determined the scheduling and agenda the previous week. To become acquainted with the system and the testing methods, we decided to test the old motors so that when the new ones arrive, we will have a rough concept of how we will be able to operate the motors and avoid harming the motor.

- **Work Progress**

I learned how to use the RCBenchmark software correctly and appropriately. It was also fresh to learn how to set up the test setup. The propulsion subsystem was unsure of the parameters required to drive the propeller as well as the amount of thrust required to propel the airship. As a result, our goal for the coming week is to determine the performance of the motors.

# Week 5

- **Agenda and Goals**

For subsystem, on Wednesday (17/11/2021), the propulsion crew returned to the propulsion lab to conduct thrust tests using the old motors with comparable specifications and the freshly ordered Tarot 4114 320KV motors. The motors' state is shown below.

For group, I do not have any role for this week.
The condition for motor 1 is non-functional because it spins improperly. The condition for motor 2 is functional and the result that we got for thrust is 1kg at current 4A.  The condition for motor 3 is functional and the result that we got for thrust is 0.48kg at current 9A but it cause a lot of vibration. Lastly, motor 4 is burn because we use unsuitable size of propeller.

- **Problem and Solution**

The propulsion subsystem has chosen to test all four of the old airship's motors. This is similar to last week, in that we need to familiarise ourselves with the software's operating system in order to get consistent results. To simplify our task when the new motors arrive, we'd want to be able to immediately install them on the test bench and execute the test with minimum complications.

- **Work Progress**

I learned how to calculate the required drag and thrust from Dr. Ezanee's explanation of the airship's equilibrium of forces this week. A possible enhancement would be to determine the appropriate throttle percentage that the motor can tolerate, since we did not know the motor's maximum performance when we fitted the 17-inch propeller.

# Week 6

- **Agenda and Goals**

For subsystem, on wednesday, we do another thrust test, this time at full throttle. On a 15.8V battery pack, the thrust achieved was around 1200gf.

- **Problem and Solution**

For subsystem, we have decided to hold a meeting before to Friday's class and a brief meeting after Friday's class. The whole team agreed that we should have the meeting to ensure that everyone is on track and not getting lost. It was an easy choice since it helps everyone. We decided to continue with the thrust test even though we do not yet have the actual motors. This was done in order for us to determine and evaluate the amount of thrust we were able to achieve after we received our new motors and ESC. Additionally, continual operation of the thrust test would allow us to familiarise ourselves with the system, ensuring that when the actual thing arrives, we can conduct the test easily and without incident. Establish that each subsystem member is on track and not falling behind. Ensure that future thrust tests are conducted smoothly (we made some mistakes for the past few weeks and all of those mistakes were taken into consideration and remembered so that we will NOT repeat the same mistakes in the future.)

- **Work Progress**

I've improved my knowledge and skills over the last few weeks and realised how critical it is to have collaboration and for each team member to understand the topic. Short meetings to reaffirm the subject at hand might be held to ensure that everyone is on the same page. As previously stated, a mistake was made during the test in which the propeller leading edge was installed incorrectly, resulting in an incorrect reading on the meters and also considerable vibration. We were able to resolve the issue. Additionally, it is important to check the connections of wires on a regular basis since one of the wires became loose and the thrust began to vary when it reached a particular number (due to strong vibration, the cable was disconnecting and connecting a little bit periodically.) We will guarantee that these errors are not repeated in the future.

# Week 7

- **Agenda and Goals**
- **Problem and Solution**
- **Work Progress**

# Week 8

- **Agenda and Goals**
- **Problem and Solution**
- **Work Progress**

# Week 9

- **Agenda and Goals**
- **Problem and Solution**
- **Work Progress**

# Week 10

- **Agenda and Goals**
- **Problem and Solution**
- **Work Progress**

# Week 11

- **Agenda and Goals**
- **Problem and Solution**
- **Work Progress**

# Week 12

- **Agenda and Goals**
- **Problem and Solution**
- **Work Progress**

# Week 13

- **Agenda and Goals**
- **Problem and Solution**
- **Work Progress**

# Week 14

- **Agenda and Goals**
- **Problem and Solution**
- **Work Progress**
